/**
 * Require the puppeteer library.
 */
const puppeteer = require("puppeteer");
const fs = require("fs");
const https = require("https");
/**
 * Require the cheerio library.
 */
const cheerio = require("cheerio");
let browser;
async function delay(ms) {
  return await new Promise((res) => setTimeout(res, ms * 1000));
}
function parseJSON(json, throwError) {
  try {
    return JSON.parse(json);
  } catch (e) {
    if (throwError) {
      throw e;
    }
  }
}
function getScriptElement(cheerioObj, type) {
  return cheerioObj('script[type="application/ld+json"]')
    .filter(function () {
      const obj = parseJSON(
        cheerioObj(this)
          .html()
          .replace(/(\r\n|\n|\r)/g, "")
      );

      return obj && obj["@type"] === type;
    })
    .map(function () {
      const obj = parseJSON(
        cheerioObj(this)
          .html()
          .replace(/(\r\n|\n|\r)/g, "")
      );
      return obj;
    })
    .get();
}

async function getStores(browserPage, topStoresOnly, override) {
  const paginationArray = ["0-9", ..."abcdefghijklmnopqrstuvwxyz".split("")];

  for (let pageNumber of paginationArray /*.slice(0,1)*/) {
    let fileName = "data/stores/stores-" + pageNumber.toString() + ".json";
    if (fs.existsSync(fileName) && !override) {
      console.log("Skipping as already exists");
      continue;
    }
    let storeData = [];
    await browserPage.goto(
      `https://www.savings.com/coupons/stores/${pageNumber}`
    );
    /**
     * Get page content as HTML.
     */
    let content = await browserPage.content();
    /**
     * Load content in cheerio.
     */
    let $ = cheerio.load(content);
    let stores = getScriptElement($, "BreadcrumbList").pop().itemListElement;
    console.log("Page Number - " + pageNumber);
    console.log("Number of stores - " + stores.length);
    if (topStoresOnly) {
      stores = stores.filter((store) => {
        return (
          $(
            `#featured-stores a[href="/coupons/${store.item["@id"].replace(
              "https://www.savings.com/coupons/",
              ""
            )}"]`
          ).length > 0
        );
      });
      console.log("Number of Top stores - " + stores.length);
    }
    for (let store of stores.slice(0, 40)) {
      try {
        await browserPage.goto(store.item["@id"]);
        content = await browserPage.content();
        $ = cheerio.load(content);
        let result = getScriptElement($, "Store");
        if (result.length > 0) {
          const { name, image, url } = getScriptElement($, "Store")[0];
          storeData.push({
            name,
            image,
            pageUrl: store.item["@id"],
            url:
              url ||
              "http://" +
                store.item["@id"].replace(
                  "https://www.savings.com/coupons/",
                  ""
                ),
          });
          console.log({
            name,
            image,
            pageUrl: store.item["@id"],
            url:
              url ||
              "http://" +
                store.item["@id"].replace(
                  "https://www.savings.com/coupons/",
                  ""
                ),
          });
        }
      } catch (e) {
        console.error("Error while getting data for " + store.item["@id"], e);
        fs.writeFileSync(fileName, JSON.stringify(storeData));
        //throw e;
        continue;
      }
    }
    fs.writeFileSync(fileName, JSON.stringify(storeData));
  }
}

async function getCouponsData(browserPage, override) {
  const files = fs.readdirSync("data/stores") || [];
  for (let file of files) {
    const dirName = "data/coupons/" + file;
    const storeFileName = "data/stores/" + file;
    if (!fs.existsSync(dirName)) {
      fs.mkdirSync(dirName);
    }
    const stores = parseJSON(fs.readFileSync(storeFileName)) || [];
    for (let store of stores) {
      const fileName =
        dirName +
        "/" +
        store.name.replace(" ", "-").replace("/", "-").toLowerCase() +
        ".json";
      console.log("Store URL - " + store.pageUrl);
      if (fs.existsSync(fileName) && !override) {
        console.log("Skipping as already exists");
        continue;
      }

      await browserPage.goto(store.pageUrl);
      content = await browserPage.content();
      let $ = cheerio.load(content);
      const data = getScriptElement($, "Organization");
      if (data && data.length > 0) {
        store.description = data[0].description;
        let coupons = data[0].hasOfferCatalog
          ? data[0].hasOfferCatalog.itemListElement || []
          : [];
        let categories = [];
        if (coupons.length > 0) {
          const couponUrl = coupons[0].item["@id"].replace("#d-", "#p-");

          await browserPage.goto(couponUrl);
          const btn = await browserPage.$(
            "#" + coupons[0].item["@id"].split("#").pop() + " .btn.orange"
          );
          await btn.click();
          browserPage.bringToFront();
          try {
            await browserPage.waitForNavigation({
              waitUntil: "domcontentloaded",
            });
            await delay(1);
            await browserPage.goBack();
          } catch (e) {
            await browserPage.reload();
          }
          $ = cheerio.load(await browserPage.content());
          categories = $("#related-categories li a")
            .map(function () {
              return $(this).text();
            })
            .get();
          coupons = coupons.map((coupon) => {
            return {
              ...coupon.item,
              couponCode: $(
                "#" + coupon.item["@id"].split("#").pop() + "  .code.block"
              ).text(),
            };
          });

          fs.writeFileSync(
            fileName,
            JSON.stringify({
              coupons,
              categories,
              store,
            })
          );
        }
      }
      //  break;
    }
    fs.writeFileSync("data/stores/" + file, JSON.stringify(stores));
    // break;
  }
}

function countStats() {
  let couponCount = 0;
  let categoryCount = 0;
  let storeCount = 0;
  const stores = fs.readdirSync("data/coupons") || [];
  stores.forEach((store) => {
    const coupons = fs.readdirSync("data/coupons/" + store);
    coupons.forEach((coupon) => {
      const arr = parseJSON(
        fs.readFileSync("data/coupons/" + store + "/" + coupon)
      );
      couponCount = couponCount + arr.coupons.length;
      storeCount = storeCount + 1;
      categoryCount = categoryCount + arr.categories.length;
    });
  });
  console.log("Coupon Count - " + couponCount);
  console.log("Store Count - " + storeCount);
  console.log("Category Count - " + categoryCount);
}

/**
 * Inside the main function we'll place all the required code
 * that will be used in the scraping process.
 * The reason why we create an async function is to use
 * the power of async programming  that comes with puppeteer.
 */
async function main() {
  /**
   * Launch Chromium. By setting `headless` key to false,
   * we can see the browser UI.
   */
  browser = await puppeteer.launch({
    headless: true,
    //  args: ['--proxy-server=socks5://127.0.0.1:9050']
  });

  /**
   * Create a new page.
   */
  const page = await browser.newPage();

  //await getStores(page, false);
  //await getCouponsData(page);
  /**
   * Wait 3 seconds and then close the browser instance.
   */
  countStats();
  setTimeout(() => {
    browser.close();
  }, 3000);
}

/**
 * Start the script by calling main().
 */
// main();

async function uploadData() {
  var WPAPI = require("wpapi");
  var wp = new WPAPI({
    endpoint: "https://couponshouse.com/wp-json",
    username: "couponshouse_user",
    password: "*23G75f*ApHZDwUhn$",
  });
  wp.stores = wp.registerRoute("wp/v2", "/stores/(?P<id>[\\d]+)");
  wp.couponCategories = wp.registerRoute(
    "wp/v2",
    "/coupon_category/(?P<id>[\\d]+)"
  );
  wp.couponTags = wp.registerRoute(
    "wp/v2",
    "/coupon_tag/(?P<id>[\\d]+)"
  );
  wp.coupons = wp.registerRoute(
    "wp/v2",
    "/coupon/(?P<id>[\\d]+)"
  );
  const createStores = async () => {
    const files = fs.readdirSync("data/stores") || [];

    for (let file of files) {
      const storeFileName = "data/stores/" + file;
      const stores = parseJSON(fs.readFileSync(storeFileName)) || [];
      console.log(
        "Uploading Stores for " + file + ", Count - " + stores.length
      );
      for (let store of stores) {
        if (!store.storeId) {
          try {
            const { id } = await wp.stores().create({
              name: store.name,
              description: store.description,
            });
            store.storeId = id;
            fs.writeFileSync(storeFileName, JSON.stringify(stores));
            console.log("Created Store - ", { ...store, storeId: id });
          } catch (e) {
            console.log({ e, store });
            throw e;
          }
        } else {
          console.log("Skipping Store - " + store.storeId);
        }
      }
    }
  };

  const createStoresMedia = async () => {
    const files = fs.readdirSync("data/stores") || [];

    for (let file of files) {
      const storeFileName = "data/stores/" + file;
      const stores = parseJSON(fs.readFileSync(storeFileName)) || [];
      console.log(
        "Uploading Stores Media for " + file + ", Count - " + stores.length
      );
      for (let store of stores) {
        if (!store.mediaId && store.image) {
          try {
            let fileName =
              "data/stores_media/" + store.image.split("?")[0].split("/").pop();
            let file = fs.createWriteStream(fileName);
            await new Promise((resolve, reject) => {
              https
                .get(store.image, function (response) {
                  response.pipe(file);
                  file.on("finish", () => {
                    file.close(resolve);
                  });
                })
                .on("error", () => {
                  fs.unlink(fileName);
                  reject();
                });
            });
            let { id } = await wp.media().file(fileName).create({
              title: store.name,
              alt_text: store.name,
              caption: store.name,
            });
            store.mediaId = id;
            fs.writeFileSync(storeFileName, JSON.stringify(stores));
            console.log("Created Store Media- ", {
              ...store,
              storeId: store.storeId,
              mediaId: id,
            });
          } catch (e) {
            console.log({ e, store });
            throw e;
          }
          // break;
        } else {
          console.log("Skipping Store - " + store.storeId);
        }
      }
      //  break;
    }
  };
  const printMediaInserts = async () => {
    const files = fs.readdirSync("data/stores") || [];
    let queries = [];
    let storeMap = parseJSON(fs.readFileSync("data/storeMap.json","utf-8")) || {};
    for (let file of files) {
      const storeFileName = "data/stores/" + file;
      const stores = parseJSON(fs.readFileSync(storeFileName)) || [];
      console.log(
        "Making inserts for " + file + ", Count - " + stores.length
      );
      for (let store of stores) {
        if (store.storeId && store.mediaId ) {
          if(!storeMap[store.name]){
            queries.push(
              `INSERT INTO couponshouse.wp_clpr_storesmeta (stores_id, meta_key, meta_value) VALUES (${store.storeId}, 'clpr_store_image_id', 'a:1:{i:0;i:${store.mediaId};}');`
            );
            queries.push(
              `INSERT INTO couponshouse.wp_clpr_storesmeta (stores_id, meta_key, meta_value) VALUES (${store.storeId}, 'clpr_store_url', '${store.url}');`
            );
            queries.push(
              `INSERT INTO couponshouse.wp_clpr_storesmeta (stores_id, meta_key, meta_value) VALUES (${store.storeId}, 'clpr_store_aff_url', '${store.url}');`
            );
          }
        } else {
          throw "No Media Id Found Store - " + store.storeId;
        }
      }
    }
    fs.writeFileSync("data/mediaQueries.sql", queries.join("\n"));
  };
  const createCouponCategories = async () => {
    const pages = fs.readdirSync("data/coupons");
    let categoryMap = fs.existsSync("data/categories.json")
      ? parseJSON(fs.readFileSync("data/categories.json", "utf-8"))
      : {};
    for (let page of pages) {
      const files = fs.readdirSync("data/coupons/" + page) || [];
      for (let file of files) {
        const storeFileName = "data/coupons/" + page + "/" + file;
        const data = parseJSON(fs.readFileSync(storeFileName)) || [];
        const { categories } = data;
        for (let category of categories) {
          if (categoryMap[category]) {
            console.log("Skipping Category - " + category);
          } else {
            try {
              const { id } = await wp.couponCategories().create({
                name: category,
                description: category,
              });
              categoryMap[category] = id;
              fs.writeFileSync(
                "data/categories.json",
                JSON.stringify(categoryMap)
              );
              console.log("Created Category - ", { category, id });
            } catch (e) {
              console.log({ e, category });
              throw e;
            }
          }
        }
      }
    }
  };
  const createCouponTags = async () => {
    const pages = fs.readdirSync("data/coupons");
    let categoryMap = fs.existsSync("data/tags.json")
      ? parseJSON(fs.readFileSync("data/tags.json", "utf-8"))
      : {};
    for (let page of pages) {
      const files = fs.readdirSync("data/coupons/" + page) || [];
      for (let file of files) {
        const storeFileName = "data/coupons/" + page + "/" + file;
        const data = parseJSON(fs.readFileSync(storeFileName)) || [];
        const { categories } = data;
        for (let category of categories) {
          if (categoryMap[category]) {
            console.log("Skipping Tag - " + category);
          } else {
            try {
              const { id } = await wp.couponTags().create({
                name: category,
                description: category,
              });
              categoryMap[category] = id;
              fs.writeFileSync(
                "data/tags.json",
                JSON.stringify(categoryMap)
              );
              console.log("Created Tag - ", { category, id });
            } catch (e) {
              console.log({ e, category });
              throw e;
            }
          }
        }
      }
    }
  };
  const createCoupons = async () => {
    const pages = fs.readdirSync("data/coupons");
    for (let page of pages) {
      const files = fs.readdirSync("data/coupons/" + page) || [];
      for (let file of files) {
        const storeFileName = "data/coupons/" + page + "/" + file;
        const data = parseJSON(fs.readFileSync(storeFileName)) || [];
        const { coupons } = data;
        for (let coupon of coupons) {
          if(coupon.couponId){
            console.log("Skipping coupon",{coupon})
          } else {
            try {
              const { id } = await wp.coupons().create({
                title: coupon.name,
                description: coupon.description,
                status: 'publish'
              });
              coupon.couponId = id;
              fs.writeFileSync(
                storeFileName,
                JSON.stringify(data)
              );
              console.log("Created Coupon - ", { ...coupon, coupond : id });
            } catch (e) {
              console.log({ e, coupon });
              throw e;
            }
          }
        }
      }
    }
  };
  const createStoreMap = async () => {
    const files = fs.readdirSync("data/stores") || [];
    let storeMap = parseJSON(fs.readFileSync("data/storeMap.json")) || {};
    for (let file of files) {
      const storeFileName = "data/stores/" + file;
      const stores = parseJSON(fs.readFileSync(storeFileName)) || [];
      for (let store of stores) {
        if (!store.storeId) {
          throw "No Store Id Found";
        } else {
          storeMap[store.name] = store.storeId;
        }
      }
    }
    console.log("Number of stores", Object.keys(storeMap).length);
    fs.writeFileSync("data/storeMap.json", JSON.stringify(storeMap));
  };
  const createUnmappedStores =  () =>{
    let storeCount = 0;
    const stores = fs.readdirSync("data/coupons") || [];
    let storeMap = parseJSON(fs.readFileSync("data/storeMap.json")) || {};
    stores.forEach((store) => {
      const coupons = fs.readdirSync("data/coupons/" + store);
      coupons.forEach((coupon) => {
        const arr = parseJSON(
          fs.readFileSync("data/coupons/" + store + "/" + coupon)
        );
        if (!storeMap[arr.store.name]) { 
          const storeJSON = parseJSON(fs.readFileSync("data/stores/" + store,"utf-8"));
          console.log("data/stores/" + store)
          storeJSON.push(arr.store);
          fs.writeFileSync("data/stores/" + store,JSON.stringify(storeJSON));
        }
      });
    });
  }

  const updateCoupons = async () => {
    let storeMap = parseJSON(fs.readFileSync("data/storeMap.json")) || {};
    let categoryMap = parseJSON(fs.readFileSync("data/categories.json")) || {};
    let tagMap = parseJSON(fs.readFileSync("data/tags.json")) || {};
    const pages = fs.readdirSync("data/coupons");
    let metaQueries = [];
    for (let page of pages) {
      const files = fs.readdirSync("data/coupons/" + page) || [];
      for (let file of files) {
        const storeFileName = "data/coupons/" + page + "/" + file;
        const data = parseJSON(fs.readFileSync(storeFileName)) || [];
        const { coupons,store,categories } = data;
        for (let coupon of coupons) {
          if(!coupon.couponId){
            console.log("Skipping coupon",{coupon})
          } else {
            try {
              // if(coupon.priceValidUntil !== 'N/A'){
              //   console.log("Updated Coupon - ", {
              //     coupon
              //   });
              //   metaQueries.push(`INSERT INTO wp_postmeta ( post_id, meta_key, meta_value) VALUES ( ${coupon.couponId}, 'clpr_expire_date', '${coupon.priceValidUntil} 00:00:00');`)
              // }
              // if(coupon.couponCode  && coupon.couponCode.trim() !== '' && coupon.couponCode !== 'N/A'){               
              //   metaQueries.push(`INSERT INTO wp_postmeta ( post_id, meta_key, meta_value) VALUES ( ${coupon.couponId}, 'clpr_coupon_code', '${coupon.couponCode}');`)
              //   metaQueries.push(`INSERT INTO wp_postmeta ( post_id, meta_key, meta_value) VALUES ( ${coupon.couponId}, 'clpr_coupon_aff_url', '${store.url}');`)
              // }
              // if(coupon.couponCode  && coupon.couponCode.trim() !== '' && coupon.couponCode !== 'N/A'){               
              //   metaQueries.push(`INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (${coupon.couponId}, 2, 0);`); }
              // if(storeMap[store.name]){
              //   metaQueries.push(`INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (${coupon.couponId}, ${storeMap[store.name]}, 0);`);
              // } else {
              //   console.log("No store found for -" , {store})

              // }
              // categories.forEach((cat)=>{
              //   if(!categoryMap[cat]){
              //     console.log("No Cat found for -" + cat)
              //     return;
              //   }
              //   metaQueries.push(`INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (${coupon.couponId}, ${categoryMap[cat]}, 0);`)
              // })
              categories.forEach((cat)=>{
                if(!tagMap[cat]){
                  console.log("No Cat found for -" + cat)
                  return;
                }
                metaQueries.push(`INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES (${coupon.couponId}, ${tagMap[cat]}, 0);`)
              })
              
            } catch (e) {
              console.log({ e, coupon });
              throw e;
            }
          }          
        }        
      }
    }
    fs.writeFileSync("data/tagRelationships.sql",metaQueries.join("\n"))
  };

  try {
    // await wp.stores().id(33,34).delete({force : true});
    // createStores();
    // createStoresMedia();
    // printMediaInserts()
    // createCouponCategories();
    // createStoreMap();
    // createUnmappedStores();
      //createCouponTags();
     // createCoupons();
     updateCoupons();
  } catch (e) {
    console.log({ e });
  }
}

uploadData();
